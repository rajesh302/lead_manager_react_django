from leads.models import Lead
from rest_framework import viewsets,permissions
from .serializers import LeadSerializer

#Lead ViewSet
#This api work at Get, Post, Put, Delete
#Get all - http://127.0.0.1:8000/api/leads/
#Get one - http://127.0.0.1:8000/api/leads/1
#Post data -http://127.0.0.1:8000/api/leads/ # with raw data
#update data with Put - http://127.0.0.1:8000/api/leads/2/ #End slash compulsary
#delete data with Delete - http://127.0.0.1:8000/api/leads/1/ # End slash compulsary

class LeadViewSet(viewsets.ModelViewSet):
    queryset = Lead.objects.all()
    permissions_classes =[permissions.AllowAny]
    serializer_class = LeadSerializer