from django.apps import AppConfig


class UiFrontendConfig(AppConfig):
    name = 'ui_frontend'
